package wipro.adi.forecast;

/**
 * Created by adi on 29/10/2017.
 */

public class StaticValues {
    public static final String API_KEY = "2156e2dd5b92590ab69c0ae1b2d24586";
    public static final String DEFAULT_CITY = "London";
    public static final String DEFAULT_LAT = "51.5072";
    public static final String DEFAULT_LON = "0.1275";
}
