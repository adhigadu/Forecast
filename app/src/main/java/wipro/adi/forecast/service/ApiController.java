package wipro.adi.forecast.service;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by adi on 30/10/2017.
 */

public class ApiController {
    private static ApiController mInstance;
    private static Context mContext;
    private RequestQueue mRequestQueue;

    private ApiController(Context context) {
        ApiController.mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized ApiController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ApiController(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(@NonNull final Request<T> request) {
        getRequestQueue().add(request);
    }

}
