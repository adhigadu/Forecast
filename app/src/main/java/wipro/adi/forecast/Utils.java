package wipro.adi.forecast;

import android.content.Context;
import android.net.ConnectivityManager;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Locale;

import static wipro.adi.forecast.StaticValues.API_KEY;
import static wipro.adi.forecast.StaticValues.DEFAULT_CITY;

/**
 * Created by adi on 29/10/2017.
 */

public class Utils {

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return (connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null) != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static URL provideURL(String[] coords) throws UnsupportedEncodingException, MalformedURLException {
        StringBuilder urlBuilder = new StringBuilder("http://api.openweathermap.org/data/2.5/");
        urlBuilder.append("forecast").append("?");
        if (coords != null && coords.length == 2) {
            urlBuilder.append("lat=").append(coords[0]).append("&lon=").append(coords[1]);
        } else {
            urlBuilder.append("q=").append(URLEncoder.encode(DEFAULT_CITY, "UTF-8"));
        }
        urlBuilder.append("&mode=json");
        urlBuilder.append("&appid=").append(API_KEY);

        return new URL(urlBuilder.toString());
    }

    public static float convertTemperature(float temperature, String unit) {
        if (unit.equals("°C")) {
            return kelvinToCelsius(temperature);
        } else if (unit.equals("°F")) {
            return kelvinToFahrenheit(temperature);
        } else {
            return temperature;
        }
    }

    public static float kelvinToCelsius(float kelvinTemp) {
        return kelvinTemp - 273.15f;
    }

    public static float kelvinToFahrenheit(float kelvinTemp) {
        return (((9 * kelvinToCelsius(kelvinTemp)) / 5) + 32);
    }

    public static String getRainString(double rain, String unit) {
        if (rain > 0) {
            if (unit.equals("mm")) {
                if (rain < 0.1) {
                    return " (<0.1 mm)";
                } else {
                    return String.format(Locale.ENGLISH, " (%.1f %s)", rain, "mm");
                }
            } else {
                rain = rain / 25.4;
                if (rain < 0.01) {
                    return " (<0.01 in)";
                } else {
                    return String.format(Locale.ENGLISH, " (%.2f %s)", rain, "mm");
                }
            }
        } else {
            return "";
        }
    }
}
