package wipro.adi.forecast.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import wipro.adi.forecast.R;
import wipro.adi.forecast.Utils;
import wipro.adi.forecast.models.Weather;

/**
 * Created by adi on 29/10/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {


    private final List<Weather> mWeatherList;

    public RecyclerViewAdapter(Context context, List<Weather> weatherList) {
        this.mWeatherList = weatherList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Weather weatherItem = mWeatherList.get(position);
        float temperature = Utils.convertTemperature(Float.parseFloat(weatherItem.getTemperature()), "°C");
        double rain = Double.parseDouble(weatherItem.getRain());
        String rainString = Utils.getRainString(rain, "mm");
        TimeZone timeZone = TimeZone.getDefault();
        String defaultDateFormat = "E dd.MM.yyyy - HH:mm";
        String dateString;
        try {
            SimpleDateFormat resultFormat = new SimpleDateFormat(defaultDateFormat);
            resultFormat.setTimeZone(timeZone);
            dateString = resultFormat.format(weatherItem.getDate());
        } catch (IllegalArgumentException e) {
            dateString = "Wrong Date Format";
        }
        holder.date.setText(dateString);
        holder.temperature.setText(new DecimalFormat("#.0").format(temperature) + " " + "°C");
        holder.description.setText(weatherItem.getDescription().substring(0, 1).toUpperCase() +
                weatherItem.getDescription().substring(1) + rainString);
    }

    @Override
    public int getItemCount() {
        return (null != mWeatherList ? mWeatherList.size() : 0);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_date)
        TextView date;
        @BindView(R.id.text_temperature)
        TextView temperature;
        @BindView(R.id.text_description)
        TextView description;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
