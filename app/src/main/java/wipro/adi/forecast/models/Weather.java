package wipro.adi.forecast.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by adi on 29/10/2017.
 */

public class Weather {

    private String mCity;
    private String mCountry;
    private Date mDate;
    private String mTemperature;
    private String mDescription;
    private String mHumidity;
    private String mIcon;
    private String mId;
    private String mRain;
    private String mDay;

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        this.mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        this.mCountry = country;
    }

    public String getTemperature() {
        return mTemperature;
    }

    public void setTemperature(String temperature) {
        this.mTemperature = temperature;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getHumidity() {
        return mHumidity;
    }

    public void setHumidity(String humidity) {
        this.mHumidity = humidity;
    }

    public String getIcon() {
        return mIcon;
    }

    public void setIcon(String icon) {
        this.mIcon = icon;
    }

    public Date getDate() {
        return this.mDate;
    }

    private void setDate(Date date) {
        this.mDate = date;
        setDay(new SimpleDateFormat("E", Locale.ENGLISH).format(date));
    }

    public void setDate(String dateString) {
        try {
            setDate(new Date(Long.parseLong(dateString) * 1000));
        } catch (Exception e) {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            try {
                setDate(inputFormat.parse(dateString));
            } catch (ParseException e2) {
                setDate(new Date());
                e2.printStackTrace();
            }
        }
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getRain() {
        return mRain;
    }

    public void setRain(String rain) {
        this.mRain = rain;
    }

    public String getDay() {
        return mDay;
    }

    public void setDay(String mDay) {
        this.mDay = mDay;
    }
}
