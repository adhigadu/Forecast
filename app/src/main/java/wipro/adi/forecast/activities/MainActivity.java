package wipro.adi.forecast.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import wipro.adi.forecast.R;
import wipro.adi.forecast.Utils;
import wipro.adi.forecast.adapters.FragmentPagerAdapter;
import wipro.adi.forecast.adapters.RecyclerViewAdapter;
import wipro.adi.forecast.fragments.MainRecyclerViewFragment;
import wipro.adi.forecast.models.Weather;
import wipro.adi.forecast.service.ApiController;

import static wipro.adi.forecast.Utils.isNetworkAvailable;
import static wipro.adi.forecast.Utils.provideURL;

/**
 * Created by adi on 29/10/2017.
 */

public class MainActivity extends AppCompatActivity implements LocationListener, MainRecyclerViewFragment.OnFragmentInteractionListener {
    protected static final int MY_ACCESS_FINE_LOCATION_REQUEST_CODE = 1;
    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.text_temperature)
    TextView mTodayTemperature;
    @BindView(R.id.text_condition)
    TextView mTodayDescription;
    @BindView(R.id.text_humidity)
    TextView mTodayHumidity;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    private LocationManager mLocationManager;
    private ProgressDialog mProgressDialog;
    private Weather mTodayWeather = new Weather();
    private List<Weather> mDay0Weather = new ArrayList<>();
    private List<Weather> mDay1Weather = new ArrayList<>();
    private List<Weather> mDay2Weather = new ArrayList<>();
    private List<Weather> mDay3Weather = new ArrayList<>();
    private List<Weather> mDay4Weather = new ArrayList<>();

    public static String getRainString(JSONObject rainObj) {
        String rain = "0";
        if (rainObj != null) {
            rain = rainObj.optString("3h", "fail");
            if ("fail".equals(rain)) {
                rain = rainObj.optString("1h", "0");
            }
        }
        return rain;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(MainActivity.this);
    }

    @Override
    public RecyclerViewAdapter getAdapter(int id) {
        RecyclerViewAdapter forecastRecyclerViewAdapter;
        switch (id) {
            case 0:
                forecastRecyclerViewAdapter = new RecyclerViewAdapter(this, mDay0Weather);
                break;
            case 1:
                forecastRecyclerViewAdapter = new RecyclerViewAdapter(this, mDay1Weather);
                break;
            case 2:
                forecastRecyclerViewAdapter = new RecyclerViewAdapter(this, mDay2Weather);
                break;
            case 3:
                forecastRecyclerViewAdapter = new RecyclerViewAdapter(this, mDay3Weather);
                break;
            case 4:
                forecastRecyclerViewAdapter = new RecyclerViewAdapter(this, mDay4Weather);
                break;
            default:
                forecastRecyclerViewAdapter = new RecyclerViewAdapter(this, mDay0Weather);
                break;
        }
        return forecastRecyclerViewAdapter;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNetworkAvailable(this)) {
            getForecastByCurrentLocation();
            //init(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mLocationManager != null) {
            try {
                mLocationManager.removeUpdates(MainActivity.this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    public void init(String[] coords) {
        if (!isNetworkAvailable(this)) {
            Toast.makeText(this, "Please check your Internet connection", Toast.LENGTH_LONG).show();
        } else {
            mProgressDialog.show();
            JsonObjectRequest jsonObjectRequest = null;
            try {
                jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                        provideURL(coords).toString(), null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            try {
                                JSONObject city = response.getJSONObject("city");
                                JSONArray list = response.getJSONArray("list");
                                JSONObject todayListItem = list.getJSONObject(0);
                                JSONObject main = todayListItem.getJSONObject("main");
                                JSONObject weather = (JSONObject) todayListItem.getJSONArray("weather").get(0);
                                mTodayWeather.setCity(city.get("name").toString());
                                mTodayWeather.setCountry(city.get("country").toString());
                                mTodayWeather.setTemperature(main.get("temp").toString());
                                mTodayWeather.setDescription(weather.get("description").toString());

                                mTodayWeather.setHumidity(main.getString("humidity"));
                                JSONObject rainObj = todayListItem.optJSONObject("rain");
                                String rain;
                                if (rainObj != null) {
                                    rain = getRainString(rainObj);
                                } else {
                                    JSONObject snowObj = todayListItem.optJSONObject("snow");
                                    if (snowObj != null) {
                                        rain = getRainString(snowObj);
                                    } else {
                                        rain = "0";
                                    }
                                }
                                mTodayWeather.setRain(rain);
                                final String idString = todayListItem.getJSONArray("weather").getJSONObject(0).getString("id");
                                mTodayWeather.setId(idString);
                                updateTodayWeatherUI();
                                parseForecastList(list);
                                mProgressDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error , try again ! ", Toast.LENGTH_LONG).show();
                                mProgressDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            mProgressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(), "Error while mLoading ... ", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismiss();
                    }
                });
            } catch (UnsupportedEncodingException | MalformedURLException e) {
                e.printStackTrace();
            }
            if (jsonObjectRequest != null) {
                ApiController.getInstance(this).addToRequestQueue(jsonObjectRequest);
            }
        }
    }

    private void updateTodayWeatherUI() {
        String city = mTodayWeather.getCity();
        String country = mTodayWeather.getCountry();
        getSupportActionBar().setTitle(city + (country.isEmpty() ? "" : ", " + country));
        float temperature = Utils.convertTemperature(Float.parseFloat(mTodayWeather.getTemperature()), "°C");
        double rain = Double.parseDouble(mTodayWeather.getRain());
        String rainString = Utils.getRainString(rain, "mm");
        mTodayTemperature.setText(new DecimalFormat("0.#").format(temperature) + " " + "°C");
        mTodayDescription.setText(mTodayWeather.getDescription().substring(0, 1).toUpperCase() +
                mTodayWeather.getDescription().substring(1) + rainString);
        mTodayHumidity.setText("Humidity" + ": " + mTodayWeather.getHumidity() + " %");
    }

    public void parseForecastList(JSONArray list) {
        try {
            mDay0Weather = new ArrayList<>();
            mDay1Weather = new ArrayList<>();
            mDay2Weather = new ArrayList<>();
            mDay3Weather = new ArrayList<>();
            mDay4Weather = new ArrayList<>();

            for (int i = 0; i < list.length(); i++) {
                Weather weather = new Weather();

                JSONObject listItem = list.getJSONObject(i);
                JSONObject main = listItem.getJSONObject("main");

                weather.setDate(listItem.getString("dt"));
                weather.setTemperature(main.getString("temp"));
                weather.setDescription(listItem.optJSONArray("weather").getJSONObject(0).getString("description"));

                JSONObject rainObj = listItem.optJSONObject("rain");
                String rain = "";
                if (rainObj != null) {
                    rain = getRainString(rainObj);
                } else {
                    JSONObject snowObj = listItem.optJSONObject("snow");
                    if (snowObj != null) {
                        rain = getRainString(snowObj);
                    } else {
                        rain = "0";
                    }
                }
                weather.setRain(rain);

                final String idString = listItem.optJSONArray("weather").getJSONObject(0).getString("id");
                weather.setId(idString);

                final String dateMsString = listItem.getString("dt") + "000";
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(Long.parseLong(dateMsString));

                Calendar today = Calendar.getInstance();

                if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) || mDay0Weather.size() <= 0) {
                    mDay0Weather.add(weather);
                } else if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) + 1) {
                    mDay1Weather.add(weather);
                } else if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) + 2) {
                    mDay2Weather.add(weather);
                } else if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) + 3) {
                    mDay3Weather.add(weather);
                } else if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) + 4) {
                    mDay4Weather.add(weather);
                }
            }
            updateForecastUI();
        } catch (JSONException e) {
            Log.e(TAG, String.valueOf(list));
            e.printStackTrace();
        }

    }

    private void updateForecastUI() {
        FragmentPagerAdapter viewPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager());

        Bundle bundleDay0 = new Bundle();
        bundleDay0.putInt("day", 0);
        MainRecyclerViewFragment recyclerViewFragmentDay0 = new MainRecyclerViewFragment();
        recyclerViewFragmentDay0.setArguments(bundleDay0);
        viewPagerAdapter.addFragment(recyclerViewFragmentDay0, mDay0Weather.get(0).getDay());

        Bundle bundleDay1 = new Bundle();
        bundleDay1.putInt("day", 1);
        MainRecyclerViewFragment recyclerViewFragmentDay1 = new MainRecyclerViewFragment();
        recyclerViewFragmentDay1.setArguments(bundleDay1);
        viewPagerAdapter.addFragment(recyclerViewFragmentDay1, mDay1Weather.get(0).getDay());

        Bundle bundleDay2 = new Bundle();
        bundleDay2.putInt("day", 2);
        MainRecyclerViewFragment recyclerViewFragmentDay2 = new MainRecyclerViewFragment();
        recyclerViewFragmentDay2.setArguments(bundleDay2);
        viewPagerAdapter.addFragment(recyclerViewFragmentDay2, mDay2Weather.get(0).getDay());

        Bundle bundleDay3 = new Bundle();
        bundleDay3.putInt("day", 3);
        MainRecyclerViewFragment recyclerViewFragmentDay3 = new MainRecyclerViewFragment();
        recyclerViewFragmentDay3.setArguments(bundleDay3);
        viewPagerAdapter.addFragment(recyclerViewFragmentDay3, mDay3Weather.get(0).getDay());

        Bundle bundleDay4 = new Bundle();
        bundleDay4.putInt("day", 4);
        MainRecyclerViewFragment recyclerViewFragmentDay4 = new MainRecyclerViewFragment();
        recyclerViewFragmentDay4.setArguments(bundleDay4);
        viewPagerAdapter.addFragment(recyclerViewFragmentDay4, mDay4Weather.get(0).getDay());

        viewPagerAdapter.notifyDataSetChanged();
        int currentPage = mViewPager.getCurrentItem();
        mViewPager.setAdapter(viewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        if (currentPage == 0 && mDay0Weather.isEmpty()) {
            currentPage = 1;
        }
        mViewPager.setCurrentItem(currentPage, true);
    }

    void getForecastByCurrentLocation() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_ACCESS_FINE_LOCATION_REQUEST_CODE);
            }

        } else if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Accessing Location");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        mLocationManager.removeUpdates(MainActivity.this);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                }
            });
            mProgressDialog.show();
            if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            }
            if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_ACCESS_FINE_LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getForecastByCurrentLocation();
                }
                return;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mProgressDialog.hide();
        try {
            mLocationManager.removeUpdates(this);
        } catch (SecurityException e) {
            Log.e(TAG, "Error while stop listening for location updates.", e);
        }
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        init(new String[]{String.valueOf(latitude), String.valueOf(longitude)});
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
